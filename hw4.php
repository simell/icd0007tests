<?php

require_once('common.php');

class Hw4Tests extends HwTests {

    function testsNotReady() {
        $this->fail('Tests for hw4 are not ready yet');
    }

}

(new Hw4Tests())->run(new PointsReporter());
